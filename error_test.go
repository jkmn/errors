package errors_test

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jkmn/errors"
)

func TestError_Basic(t *testing.T) {

	err := errors.New("test error")

	t.Log(err.Error())
	t.Log(err.Root())
	t.Log(err.Stack())
}

func TestError_MarshalJSON(t *testing.T) {
	a := assert.New(t)

	err := errors.New("test error")

	b := bytes.NewBuffer(nil)
	enc := json.NewEncoder(b)
	enc.SetIndent("", "\t")
	err_ := enc.Encode(err)
	a.NoError(err_)

	t.Log(b.String())
}
