fmt:
	for d in $(shell go list -f {{.Dir}} ./...); do goimports -w $$d/*.go; done

test: fmt
	go test -v ./...
	go vet -composites=false $$(go list ./... | grep -v /vendor/)
	golangci-lint run ./...

upgrade:
	go get -u -t all

vendor:
	go mod tidy -go=1.17
	go mod vendor
	go mod verify

.PHONY: \
	fmt \
	test \
	upgrade \
	vendor \