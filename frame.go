package errors

import (
	"fmt"
	"runtime"
	"strings"
)

type frame struct {
	Func string `json:"func"`
	Line int    `json:"line"`
	File string `json:"file"`
}

func getFrames() []frame {
	pc := make([]uintptr, 5)
	runtime.Callers(3, pc)
	cFrames := runtime.CallersFrames(pc)

	out := make([]frame, 0)

	cf, more := cFrames.Next()
	for {
		if (cf == runtime.Frame{}) {
			break
		}

		funcChunks := strings.Split(cf.Func.Name(), "/")
		funcName := funcChunks[len(funcChunks)-1]

		f := frame{
			Func: funcName,
			Line: cf.Line,
			File: cf.File,
		}
		out = append(out, f)

		if !more {
			break
		}
		cf, more = cFrames.Next()
	}

	return out
}

func (f frame) String() string {
	return fmt.Sprintf("%s:%d\t(%s)\n", f.File, f.Line, f.Func)
}
